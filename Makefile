.PHONY: all release clean
all: roll-any.exe
release: roll-any.zip

roll-any.zip: roll-any
	7z a roll-any.zip roll-any
	rm -rf roll-any

roll-any: roll-any.exe
	mkdir roll-any
	mv plug-ins roll-any
	mv roll-any.exe roll-any
	cp roll-any.nako roll-any
	cp LICENSE.txt roll-any
	cp README.mkd roll-any
	cp Makefile roll-any

roll-any.exe: roll-any.nako
	nakomake roll-any.nako -t vnako -p dnako -p nakoctrl -p nakofile

clean:
	rm -rf plug-ins roll-any roll-any.exe roll-any.zip
